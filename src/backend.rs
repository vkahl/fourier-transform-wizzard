// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::{
    path::PathBuf,
    sync::{
        mpsc::{Receiver, SyncSender},
        Arc,
    },
};

use realfft::{num_complex::Complex, ComplexToReal};
use thin_vec::{thin_vec, ThinVec};
use tracing::{debug, info, trace};

const MAX_ALLOWED_FRAMES: usize = 65_536;

//
// =============================================================================
//

#[derive(Debug, PartialEq)]
pub enum Request {
    DroppedFile(PathBuf),
    NextNode,
    PrevNode,
    JumpForward,
    JumpBack,
    FirstNode,
    LastNode,
}

#[derive(Debug)]
pub enum Response {
    LoadingFile(PathBuf),
    ErrorLoadingFile { file: PathBuf, error: String },
    ProcessingFile(PathBuf),
    LoadedAndProcessedFile { _file: PathBuf, plot: Plot },
    ErrorProcessingFile { file: PathBuf, error: realfft::FftError },
    GeneratingPlot(Node),
    NewPlot(Plot),
}

//
// =============================================================================
//

pub fn spawn(ctx: eframe::egui::Context, tx: SyncSender<Response>, rx: Receiver<Request>) {
    std::thread::spawn(move || {
        let mut core = AppCore::new(ctx, tx);

        while let Ok(request) = rx.recv() {
            core.handle(request);
        }

        info!("Shutting down backend thread");
    });
}

//
// =============================================================================
//

#[derive(Debug)]
struct AppCore {
    ctx: eframe::egui::Context,
    tx: SyncSender<Response>,

    audio_file: Option<AudioFile>,
    current_node_index: usize,
}

impl AppCore {
    fn new(ctx: eframe::egui::Context, tx: SyncSender<Response>) -> Self {
        Self { ctx, tx, audio_file: None, current_node_index: 0 }
    }

    fn handle(&mut self, request: Request) {
        trace!("Handling request: {request:?}");

        match request {
            Request::DroppedFile(file) => self.dropped_file(file),
            Request::NextNode => {
                self.current_node_index = self.current_node_index.saturating_add(1);
                self.try_update_plot();
            }
            Request::PrevNode => {
                self.current_node_index = self.current_node_index.saturating_sub(1);
                self.try_update_plot();
            }
            Request::JumpForward => {
                if let Some(jump_size) = self.audio_file.as_ref().map(|af| af.jump_size) {
                    self.current_node_index = self.current_node_index.saturating_add(jump_size);
                    self.try_update_plot();
                }
            }
            Request::JumpBack => {
                if let Some(jump_size) = self.audio_file.as_ref().map(|af| af.jump_size) {
                    self.current_node_index = self.current_node_index.saturating_sub(jump_size);
                    self.try_update_plot();
                }
            }
            Request::FirstNode => {
                self.current_node_index = 0;
                self.try_update_plot();
            }
            Request::LastNode => {
                self.current_node_index = usize::MAX;
                self.try_update_plot();
            }
        }
    }

    fn dropped_file(&mut self, file: PathBuf) {
        debug!("Handling dropped file: {file:?}");

        self.send_frontend(Response::LoadingFile(file.clone()));

        let mut wav = match wavers::Wav::<f32>::from_path(&file) {
            Ok(wav) => wav,
            Err(error) => {
                self.send_frontend(Response::ErrorLoadingFile { file, error: error.to_string() });
                return;
            }
        };
        let n_channels = u32::from(wav.n_channels());
        if n_channels != 1 {
            self.send_frontend(Response::ErrorLoadingFile {
                file,
                error: "The provided wav file has got an unsupported number of channels. The file \
                    has to be mono."
                    .into(),
            });
            return;
        }

        if wav.n_samples() > MAX_ALLOWED_FRAMES {
            self.send_frontend(Response::ErrorLoadingFile {
                file,
                error: "The provided wav file is too long. It must contain only one channel with \
                    at most 65536 samples."
                    .into(),
            });
            return;
        }
        let samples = match wav.read() {
            Ok(samples) => samples,
            Err(error) => {
                self.send_frontend(Response::ErrorLoadingFile { file, error: error.to_string() });
                return;
            }
        };
        let sample_rate = wav.sample_rate();

        self.send_frontend(Response::ProcessingFile(file.clone()));

        self.current_node_index = 0;
        let mut planner = realfft::RealFftPlanner::<f32>::new();
        let fft = planner.plan_fft_forward(samples.len());
        let samples: ThinVec<f32> = samples.iter().copied().collect();
        let mut indata = samples.clone();
        let mut spectrum = thin_vec![Complex::from(0.); fft.complex_len()];
        let mut scratch: ThinVec<_> = thin_vec![Complex::default(); fft.get_scratch_len()];
        trace!("Calculating full spectrum");
        if let Err(error) = fft.process_with_scratch(&mut indata, &mut spectrum, &mut scratch) {
            self.send_frontend(Response::ErrorProcessingFile { file, error });
            return;
        }

        let ifft_plan = planner.plan_fft_inverse(samples.len());
        let inverse_indata = thin_vec![Complex::from(0.); fft.complex_len()];
        let inverse_outdata = thin_vec![0.; samples.len()];

        #[allow(clippy::cast_sign_loss)]
        let jump_size = ((spectrum.len() as f32 / 100.).round() as usize).max(1);
        let mut audio_file = AudioFile {
            path: file.clone(),
            sample_rate: sample_rate as f32,
            samples: Arc::new(samples),
            full_spectrum: spectrum,
            inverse_indata,
            inverse_outdata,
            scratch,
            ifft_plan,
            jump_size,
        };

        let plot = match self.generate_plot(&mut audio_file) {
            Ok(plot) => plot,
            Err(error) => {
                self.send_frontend(Response::ErrorProcessingFile { file, error });
                return;
            }
        };
        self.audio_file = Some(audio_file);
        self.send_frontend(Response::LoadedAndProcessedFile { _file: file, plot });
    }

    fn try_update_plot(&mut self) {
        if let Some(mut audio_file) = self.audio_file.take() {
            let plot = match self.generate_plot(&mut audio_file) {
                Ok(plot) => plot,
                Err(error) => {
                    self.send_frontend(Response::ErrorProcessingFile {
                        file: audio_file.path,
                        error,
                    });
                    return;
                }
            };
            self.audio_file = Some(audio_file);
            self.send_frontend(Response::NewPlot(plot));
        }
    }

    fn generate_plot(&mut self, audio_file: &mut AudioFile) -> Result<Plot, realfft::FftError> {
        self.current_node_index = self.current_node_index.min(audio_file.full_spectrum.len());
        let (node, current_component, current_reconstruction) =
            if let Some(node) = audio_file.get_node(self.current_node_index) {
                self.send_frontend(Response::GeneratingPlot(node));

                audio_file.inverse_indata.fill(0_f32.into());
                audio_file.inverse_indata[self.current_node_index] =
                    audio_file.full_spectrum[self.current_node_index];
                trace!("Calculating current component");
                audio_file.ifft_plan.process_with_scratch(
                    &mut audio_file.inverse_indata,
                    &mut audio_file.inverse_outdata,
                    &mut audio_file.scratch,
                )?;
                let scale = (audio_file.samples.len() as f32).recip();
                let current_component: ThinVec<f32> =
                    audio_file.inverse_outdata.iter().map(|f| f * scale).collect();
                trace!("Calculating current reconstruction");
                audio_file.inverse_indata.fill(0_f32.into());
                audio_file.inverse_indata[..self.current_node_index]
                    .copy_from_slice(&audio_file.full_spectrum[..self.current_node_index]);
                audio_file.ifft_plan.process_with_scratch(
                    &mut audio_file.inverse_indata,
                    &mut audio_file.inverse_outdata,
                    &mut audio_file.scratch,
                )?;
                let current_reconstruction: ThinVec<f32> =
                    audio_file.inverse_outdata.iter().map(|f| f * scale).collect();
                (node, current_component, current_reconstruction)
            } else {
                let node = Node {
                    index: self.current_node_index,
                    frequency: f32::INFINITY,
                    magnitude: 0.,
                    phase: 0.,
                };

                self.send_frontend(Response::GeneratingPlot(node));

                (node, thin_vec![0.; audio_file.samples.len()], (*audio_file.samples).clone())
            };

        Ok(Plot {
            node,
            target_signal: Arc::clone(&audio_file.samples),
            current_reconstruction,
            current_component,
        })
    }

    fn send_frontend(&self, response: Response) {
        self.tx.send(response).ok();
        self.ctx.request_repaint();
    }
}

struct AudioFile {
    path: PathBuf,
    sample_rate: f32,
    samples: Arc<ThinVec<f32>>,
    full_spectrum: ThinVec<Complex<f32>>,
    inverse_indata: ThinVec<Complex<f32>>,
    inverse_outdata: ThinVec<f32>,
    scratch: ThinVec<Complex<f32>>,
    ifft_plan: Arc<dyn ComplexToReal<f32>>,
    jump_size: usize,
}

impl std::fmt::Debug for AudioFile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("AudioFile")
            .field("sample_rate", &self.sample_rate)
            .field("samples", &self.samples)
            .field("full_spectrum", &self.full_spectrum)
            .field("inverse_indata", &self.inverse_indata)
            .field("inverse_outdata", &self.inverse_outdata)
            .finish_non_exhaustive()
    }
}

impl AudioFile {
    fn get_node(&self, index: usize) -> Option<Node> {
        let scale = (self.samples.len() as f32).recip();
        let frequency = index as f32 * scale * self.sample_rate;
        self.full_spectrum.get(index).map(|c| {
            let magnitude_lin = if self.samples.len() % 2 == 0
                && index == self.full_spectrum.len().saturating_sub(1)
            {
                // The signal has got an even number of samples and this is the
                // very last node (nyquist frequency). In this case, the
                // magnitude has to be cut in half.
                c.norm() * scale
            } else {
                c.norm() * scale * 2.
            };
            let magnitude = 20. * (magnitude_lin).log10();
            let phase = c.arg().to_degrees();
            Node { index, frequency, magnitude, phase }
        })
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Node {
    pub index: usize,
    pub frequency: f32,
    pub magnitude: f32,
    pub phase: f32,
}

pub struct Plot {
    pub node: Node,
    pub target_signal: Arc<ThinVec<f32>>,
    pub current_reconstruction: ThinVec<f32>,
    pub current_component: ThinVec<f32>,
}

impl std::fmt::Debug for Plot {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Plot").field("node", &self.node).finish_non_exhaustive()
    }
}
