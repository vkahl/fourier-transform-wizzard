// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::sync::OnceLock;

use eframe::{
    egui::{include_image, Context, Frame, Grid, Image, RichText, ScrollArea, Ui, Window},
    emath::Align2,
};
use egui_commonmark::{CommonMarkCache, CommonMarkViewer};
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};

use crate::{MIN_WINDOW_HEIGHT, MIN_WINDOW_WIDTH};

//
// =============================================================================
//

const ABOUT_CONTENT_SIZE: [f32; 2] = [MIN_WINDOW_WIDTH * 0.618, MIN_WINDOW_HEIGHT - 50.];

fn about_logo() -> &'static Image<'static> {
    pub static ABOUT_LOGO: OnceLock<Image<'static>> = OnceLock::new();
    ABOUT_LOGO.get_or_init(|| Image::new(include_image!("../../gitignore/about.png")))
}

pub const CHANGELOG_MARKDOWN: &str = include_str!("../../CHANGELOG.md");

//
// =============================================================================
//

pub fn nav_help_window(open: &mut bool, ctx: &Context) {
    Window::new("Navigation help")
        .collapsible(false)
        .scroll2([false; 2])
        .resizable(false)
        .title_bar(true)
        .open(open)
        .frame(Frame::popup(&ctx.style()).fill(ctx.style().visuals.extreme_bg_color))
        .anchor(Align2::CENTER_CENTER, [0.; 2])
        .show(ctx, |ui| {
            let y_padding = ctx.style().spacing.window_margin.top;

            ui.strong("Arrow keys up/down or left/right");
            ui.label("Show previous/next step");
            ui.add_space(y_padding);

            ui.strong("Page up/down");
            ui.label("Skip 1% of steps back/ahead");
            ui.add_space(y_padding);

            ui.strong("Home/end");
            ui.label("Skip to first/last step");
            ui.add_space(y_padding);

            ui.separator();
            ui.add_space(y_padding);

            ui.strong("Scroll-wheel");
            ui.label("Move plot axes up/down");
            ui.add_space(y_padding);

            ui.strong("Shift + scroll-wheel");
            ui.label("Move plot axes left/right");
            ui.add_space(y_padding);

            ui.strong("Ctrl/Cmd + scroll-wheel");
            ui.label("Zoom plot axes");
            ui.add_space(y_padding);

            ui.strong("Drag mouse (left click)");
            ui.label("Move plot axes");
            ui.add_space(y_padding);

            ui.strong("Drag mouse (right click)");
            ui.label("Zoom to area");
            ui.add_space(y_padding);

            ui.strong("Double-click");
            ui.label("Reset zoom and axes position");
        });
}

//
// =============================================================================
//

fn changelog_window_inner(ui: &mut Ui) {
    const CHANGELOG_WINDOW_SIZE: [f32; 2] = [MIN_WINDOW_WIDTH * 0.8, MIN_WINDOW_HEIGHT * 0.8];
    let mut cache = CommonMarkCache::default();
    ScrollArea::new([false, true])
        .max_width(CHANGELOG_WINDOW_SIZE[0])
        .max_height(CHANGELOG_WINDOW_SIZE[1])
        .auto_shrink([false; 2])
        .show(ui, |ui| {
            #[allow(clippy::cast_sign_loss)]
            CommonMarkViewer::new("changelog viewer")
                .default_width(Some(CHANGELOG_WINDOW_SIZE[0] as usize))
                .max_image_width(Some(100))
                .show(ui, &mut cache, CHANGELOG_MARKDOWN);
        });
}

pub fn changelog_window(open: &mut bool, ctx: &Context) {
    Window::new("Changelog")
        .open(open)
        .collapsible(false)
        .scroll2([false; 2])
        .resizable(false)
        .title_bar(true)
        .frame(Frame::popup(&ctx.style()).fill(ctx.style().visuals.extreme_bg_color))
        .anchor(Align2::CENTER_CENTER, [0.; 2])
        .show(ctx, |ui| {
            changelog_window_inner(ui);
        });
}

//
// =============================================================================
//

fn about_window_inner(ctx: &Context, ui: &mut Ui) {
    Grid::new("about grid").spacing(ctx.style().spacing.item_spacing).show(ui, |ui| {
        ui.vertical(|ui| {
            ui.vertical_centered(|ui| {
                ui.add(about_logo().clone().fit_to_original_size(0.25));
            });
            ui.vertical_centered(|ui| {
                ui.label(RichText::new(std::env!("LONG_NAME")).strong());
            });
            ui.separator();
        });

        ui.end_row();

        Grid::new("about lower").spacing(ctx.style().spacing.item_spacing).show(ui, |ui| {
            Grid::new("about lower left")
                .max_col_width(ABOUT_CONTENT_SIZE[0] * 0.5)
                .spacing([0., ctx.style().spacing.icon_spacing])
                .show(ui, |ui| {
                    ui.label(RichText::new("Version:").underline());
                    ui.end_row();
                    ui.label(std::env!("CARGO_PKG_VERSION"));

                    ui.end_row();
                    ui.label(RichText::new("Author:").underline());
                    ui.end_row();
                    ui.label(std::env!("CARGO_PKG_AUTHORS"));

                    let repo_url = std::env!("CARGO_PKG_REPOSITORY");
                    if !repo_url.is_empty() {
                        ui.end_row();
                        ui.label(RichText::new("Repository:").underline());
                        ui.end_row();
                        ui.hyperlink(repo_url);
                    }

                    ui.end_row();
                    ui.label(RichText::new("License:").underline());
                    ui.end_row();
                    let license_text = format!(
                        "{} is made available to you under the terms of the Mozilla Public \
                        License (MPL-2.0)",
                        std::env!("DISPLAY_NAME"),
                    );
                    ui.label(license_text);
                    ui.end_row();
                    ui.hyperlink("https://www.mozilla.org/en-US/MPL/2.0");
                });

            Frame::group(&ctx.style())
                .fill(ctx.style().visuals.widgets.noninteractive.bg_fill)
                .show(ui, |ui| {
                    ui.vertical(|ui| {
                        ui.label(RichText::new("Dependencies").underline());
                        ui.add_space(ctx.style().spacing.icon_spacing);
                        ScrollArea::vertical().show(ui, |ui| {
                            Grid::new("dependencies")
                                .max_col_width(
                                    ABOUT_CONTENT_SIZE[0] * 0.25
                                        - ctx.style().spacing.item_spacing.y,
                                )
                                .spacing(ctx.style().spacing.item_spacing)
                                .striped(true)
                                .show(ui, |ui| {
                                    for dep in DEPENDENCIES
                                        .iter()
                                        .filter(|dep| dep.name != std::env!("CARGO_PKG_NAME"))
                                    {
                                        if dep.repository().is_empty() {
                                            ui.label(&dep.name);
                                        } else {
                                            ui.hyperlink_to(&dep.name, dep.repository());
                                        }
                                        if dep.license_file().is_empty() {
                                            ui.label(dep.license());
                                        } else {
                                            ui.hyperlink_to(dep.license(), dep.license_file());
                                        }
                                        ui.end_row();
                                    }
                                });
                        });
                    });
                });
        });
    });
}

pub fn about_window(open: &mut bool, ctx: &Context) {
    Window::new(format!("About {}", std::env!("DISPLAY_NAME")))
        .open(open)
        .collapsible(false)
        .scroll2([false; 2])
        .resizable(false)
        .title_bar(true)
        .frame(Frame::popup(&ctx.style()).fill(ctx.style().visuals.extreme_bg_color))
        .anchor(Align2::CENTER_CENTER, [0.; 2])
        .show(ctx, |ui| {
            about_window_inner(ctx, ui);
        });
}

//
// =============================================================================
//

static DEPENDENCIES: Lazy<Vec<Dependency>> = Lazy::new(|| {
    let mut deps_vec: Vec<Dependency> =
        serde_json::from_str(include_str!("../../gitignore/dependencies.json")).unwrap();
    deps_vec.dedup_by(|a, b| a.name == b.name);
    deps_vec
});

#[derive(Debug, Serialize, Deserialize)]
struct Dependency {
    pub name: String,
    pub version: Option<String>,
    pub authors: Option<String>,
    pub repository: Option<String>,
    pub license: Option<String>,
    pub license_file: Option<String>,
    pub description: Option<String>,
}

impl Dependency {
    pub fn repository(&self) -> &str {
        match &self.repository {
            Some(value) => value,
            None => "",
        }
    }
    pub fn license(&self) -> &str {
        match &self.license {
            Some(value) => value,
            None => "",
        }
    }
    pub fn license_file(&self) -> &str {
        match &self.license_file {
            Some(value) => value,
            None => "",
        }
    }
}
