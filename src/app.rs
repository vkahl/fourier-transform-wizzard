// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use std::{
    path::PathBuf,
    sync::mpsc::{Receiver, SyncSender},
};

use eframe::{
    egui::{
        gui_zoom::zoom_menu_buttons, menu, CentralPanel, Context, Id, InputState, Key,
        KeyboardShortcut, LayerId, Layout, Modifiers, Order, RichText, Slider, TextStyle,
        TopBottomPanel, Ui, ViewportCommand, Visuals,
    },
    emath::{Align, Align2},
    epaint::Color32,
};
use egui_modal::{Icon, Modal};
use enum_iterator::{all, Sequence};
use tracing::{error, info};

use crate::{
    backend::{Plot, Request, Response},
    settings::GeneralSettings,
};

mod help;

//
// =============================================================================
//

#[derive(Debug)]
#[allow(clippy::struct_excessive_bools)]
pub struct App {
    general_settings: GeneralSettings,

    backend: Backend,

    spinner: bool,
    current_plot: Option<Plot>,
    current_view: CurrentView,

    about_window: bool,
    changelog_window: bool,
    nav_help_window: bool,
}

impl App {
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        let ctx = &cc.egui_ctx;

        // Install image loader
        egui_extras::install_image_loaders(ctx);

        let general_settings = load_app_settings().unwrap_or_default();

        let app = Self {
            general_settings,
            backend: Backend::spawn(ctx.clone()),
            spinner: false,
            current_plot: None,
            current_view: CurrentView::default(),
            about_window: false,
            changelog_window: false,
            nav_help_window: false,
        };

        // Directly open the file if one has been passed
        if let Some(wav_file_path) = std::env::args().nth(1) {
            app.send_backend(Request::DroppedFile(wav_file_path.into()));
        }

        app.apply_dark_mode(ctx);

        app
    }

    #[allow(clippy::too_many_lines)]
    fn main_view(&mut self, ui: &mut Ui, error_dialog: &Modal) {
        if let Some(plot) = &self.current_plot {
            let heading_line_height = ui.text_style_height(&TextStyle::Heading);
            let normal_line_height = ui.text_style_height(&TextStyle::Body);
            let total_height = heading_line_height + ui.style().spacing.item_spacing.y;

            let is_last_node = plot.node.frequency.is_infinite();

            if self.spinner {
                let margin_y = (total_height - normal_line_height) * 0.5;
                ui.vertical_centered(|ui| {
                    ui.set_height(total_height);
                    ui.add_space(margin_y);
                    ui.spinner();
                });
            } else if is_last_node {
                // Last node
                let margin_y = (total_height - normal_line_height) * 0.5;
                ui.vertical_centered(|ui| {
                    ui.set_height(total_height);
                    ui.add_space(margin_y);
                    ui.label("There are no more frequencies to add.");
                });
            } else {
                let width_per_field = 200.;
                let index_field_width = 150.;
                let total_width = width_per_field * 3.
                    + index_field_width
                    + ui.style().spacing.item_spacing.x * 11.;
                let margin_x = (ui.available_width() - total_width) * 0.5;

                ui.horizontal(|ui| {
                    ui.set_height(total_height);
                    ui.add_space(margin_x);
                    ui.separator();
                    ui.horizontal(|ui| {
                        ui.set_width(index_field_width);
                        ui.heading("Index:");
                        ui.with_layout(Layout::right_to_left(Align::TOP), |ui| {
                            ui.heading(RichText::new(format!("{}", plot.node.index)).strong());
                        });
                    });
                    ui.separator();
                    ui.horizontal(|ui| {
                        ui.set_width(width_per_field);
                        ui.heading("Frequency:");
                        ui.with_layout(Layout::right_to_left(Align::TOP), |ui| {
                            ui.heading(RichText::new("Hz"));
                            ui.heading(
                                RichText::new(format!("{:.2}", plot.node.frequency)).strong(),
                            );
                        });
                    });
                    ui.separator();
                    ui.horizontal(|ui| {
                        ui.set_width(width_per_field);
                        ui.heading("Magnitude:");
                        ui.with_layout(Layout::right_to_left(Align::TOP), |ui| {
                            ui.heading(RichText::new("dB"));
                            ui.heading(
                                RichText::new(format!("{:.2}", plot.node.magnitude)).strong(),
                            );
                        });
                    });
                    ui.separator();
                    ui.horizontal(|ui| {
                        ui.set_width(width_per_field);
                        ui.heading("Phase:");
                        ui.with_layout(Layout::right_to_left(Align::TOP), |ui| {
                            ui.heading(RichText::new("° "));
                            ui.heading(RichText::new(format!("{:.2}", plot.node.phase)).strong());
                        });
                    });
                    ui.separator();
                });
            }

            let target_signal =
                egui_plot::Line::new(egui_plot::PlotPoints::from_ys_f32(&plot.target_signal))
                    .width(self.general_settings.line_width)
                    .color(Color32::from_rgb(95, 141, 211));
            let current_component =
                egui_plot::Line::new(egui_plot::PlotPoints::from_ys_f32(&plot.current_component))
                    .width(self.general_settings.line_width)
                    .color(Color32::from_rgb(230, 184, 0));
            let current_reconstruction = egui_plot::Line::new(egui_plot::PlotPoints::from_ys_f32(
                &plot.current_reconstruction,
            ))
            .width(self.general_settings.line_width)
            .color(Color32::from_rgb(191, 48, 48));
            egui_plot::Plot::new("plot").include_y(1.3).include_y(-1.3).show(ui, |plot_ui| {
                plot_ui.line(target_signal);
                plot_ui.line(current_reconstruction);
                if !is_last_node {
                    plot_ui.line(current_component);
                }
            });
        } else {
            let height = ui.available_height();
            let line_height = ui.text_style_height(&TextStyle::Body);
            if self.spinner {
                let y_margin = (height) / 2. - line_height;
                ui.vertical_centered(|ui| {
                    ui.add_space(y_margin);
                    ui.spinner();
                    ui.label("Loading and analyzing file");
                });
                return;
            }

            let y_margin = (height - line_height) / 2.;
            ui.vertical_centered(|ui| {
                ui.add_space(y_margin);
                ui.label("Drag and drop a single wav file");
            });
        }

        fn is_wav_file(file_path: &std::path::Path) -> bool {
            file_path.extension().is_some_and(|ext| ext.eq_ignore_ascii_case("wav"))
        }
        match Self::file_drop(ui.ctx()).as_slice() {
            files @ [file] if files.len() == 1 && is_wav_file(file) => {
                self.send_backend(Request::DroppedFile(file.clone()));
            }
            [] => (),
            _ => modal_error(error_dialog, "Please drop only a single wav-file"),
        };
    }

    fn file_drop(ctx: &Context) -> Vec<PathBuf> {
        let mut files = vec![];

        if !ctx.input(|i| i.raw.hovered_files.is_empty()) {
            let text = "drop files";
            let painter = ctx.layer_painter(LayerId::new(Order::Foreground, Id::new("file drop")));
            let screen_rect = ctx.input(InputState::screen_rect);
            let bg_overlay_color = Color32::from_black_alpha(250);
            painter.rect_filled(screen_rect, 0.0, bg_overlay_color);
            painter.text(
                screen_rect.center(),
                Align2::CENTER_CENTER,
                text,
                TextStyle::Heading.resolve(&ctx.style()),
                Color32::WHITE,
            );
        }

        ctx.input(|i| {
            let dropped_files = &i.raw.dropped_files;
            if !dropped_files.is_empty() {
                for dropped_file in dropped_files {
                    if let Some(dropped_file_path) = &dropped_file.path {
                        files.push(dropped_file_path.clone());
                    }
                }
            }
        });

        files
    }

    fn send_backend(&self, request: Request) {
        self.backend.tx.send(request).expect("Backend panic");
    }

    fn apply_dark_mode(&self, ctx: &Context) {
        let visuals = match self.general_settings.dark_mode {
            crate::settings::DarkModeSettings::System => match dark_light::detect() {
                dark_light::Mode::Dark => Visuals::dark(),
                dark_light::Mode::Light | dark_light::Mode::Default => Visuals::light(),
            },
            crate::settings::DarkModeSettings::On => Visuals::dark(),
            crate::settings::DarkModeSettings::Off => Visuals::light(),
        };
        ctx.set_visuals(visuals);
    }

    fn close_all_subwindows(&mut self) {
        self.nav_help_window = false;
        self.about_window = false;
        self.changelog_window = false;
    }
}

impl eframe::App for App {
    #[allow(clippy::too_many_lines)]
    fn update(&mut self, ctx: &eframe::egui::Context, _frame: &mut eframe::Frame) {
        self.apply_dark_mode(ctx);

        let mut error_dialog = Modal::new(ctx, "Error");
        error_dialog.show_dialog();

        // Handle messages from backend
        while let Ok(response) = self.backend.rx.try_recv() {
            info!("Response: {response:?}");

            match response {
                Response::LoadingFile(_file) => {
                    self.current_plot = None;
                    self.spinner = true;
                }
                Response::ErrorLoadingFile { file, error } => {
                    self.spinner = false;
                    modal_error(&error_dialog, &format!("Can't read file: {file:?} ({error})"));
                }
                Response::ProcessingFile(_file) => {
                    self.spinner = true;
                }
                Response::ErrorProcessingFile { file, error } => {
                    self.spinner = false;
                    modal_error(
                        &error_dialog,
                        &format!("Error processing file: {file:?} ({error})"),
                    );
                }
                Response::LoadedAndProcessedFile { _file: _, plot } => {
                    self.spinner = false;
                    self.current_plot = Some(plot);
                }
                Response::GeneratingPlot(_node) => self.spinner = true,
                Response::NewPlot(plot) => {
                    self.current_plot = Some(plot);
                    self.spinner = false;
                }
            }
        }

        ctx.input(|input| {
            if input.viewport().close_requested() {
                try_save_app_settings(&self.general_settings);
            }
        });

        ctx.input_mut(|input| {
            if input.consume_shortcut(&KeyboardShortcut {
                modifiers: Modifiers::COMMAND,
                logical_key: Key::Q,
            }) {
                ctx.send_viewport_cmd(ViewportCommand::Close);
            } else if input.key_pressed(Key::ArrowDown) || input.key_pressed(Key::ArrowRight) {
                self.spinner = true;
                self.send_backend(Request::NextNode);
            } else if input.key_pressed(Key::ArrowUp) || input.key_pressed(Key::ArrowLeft) {
                self.spinner = true;
                self.send_backend(Request::PrevNode);
            } else if input.key_pressed(Key::PageDown) {
                self.spinner = true;
                self.send_backend(Request::JumpForward);
            } else if input.key_pressed(Key::PageUp) {
                self.spinner = true;
                self.send_backend(Request::JumpBack);
            } else if input.key_pressed(Key::Home) {
                self.spinner = true;
                self.send_backend(Request::FirstNode);
            } else if input.key_pressed(Key::End) {
                self.spinner = true;
                self.send_backend(Request::LastNode);
            } else if input.key_pressed(Key::Escape) {
                self.close_all_subwindows();
            }
        });

        TopBottomPanel::top("menu bar").show(ctx, |ui| {
            menu::bar(ui, |ui| {
                ui.menu_button("File", |ui| {
                    if ui.button("Quit").clicked() {
                        ctx.send_viewport_cmd(ViewportCommand::Close);
                    }
                });
                ui.menu_button("View", |ui| {
                    ui.add(
                        Slider::new(&mut self.general_settings.line_width, 1_f32..=10_f32)
                            .text("Plot line width"),
                    );
                    ui.separator();
                    zoom_menu_buttons(ui);
                    ui.separator();
                    if ui
                        .button(format!("Dark mode ({})", self.general_settings.dark_mode))
                        .clicked()
                    {
                        self.general_settings.dark_mode.toggle();
                    }
                });
                if CurrentView::CARDINALITY > 1 {
                    ui.menu_button("Window", |ui| {
                        for view in all::<CurrentView>() {
                            let mut label = RichText::new(view.to_string());
                            if self.current_view == view {
                                label = label.underline();
                            }
                            if ui.button(label).clicked() {
                                ui.close_menu();
                                self.current_view = view;
                            }
                        }
                    });
                }
                ui.menu_button("Help", |ui| {
                    if ui.button("Navigation").clicked() {
                        ui.close_menu();
                        self.close_all_subwindows();
                        self.nav_help_window = true;
                    }
                    ui.separator();
                    if ui.button("Changelog").clicked() {
                        ui.close_menu();
                        self.close_all_subwindows();
                        self.changelog_window = true;
                    }
                    if ui.button(format!("About {}", std::env!("DISPLAY_NAME"))).clicked() {
                        ui.close_menu();
                        self.close_all_subwindows();
                        self.about_window = true;
                    }
                });
            });
        });

        CentralPanel::default().show(ctx, |ui| {
            help::nav_help_window(&mut self.nav_help_window, ctx);
            help::about_window(&mut self.about_window, ctx);
            help::changelog_window(&mut self.changelog_window, ctx);

            match self.current_view {
                CurrentView::Main => {
                    self.main_view(ui, &error_dialog);
                }
            };
        });
    }

    fn persist_egui_memory(&self) -> bool {
        false
    }
}

//
// =============================================================================
//

#[derive(Debug, Clone, Copy, PartialEq, Default, Sequence, derive_more::Display)]
enum CurrentView {
    #[default]
    Main,
}

#[derive(Debug)]
struct Backend {
    tx: SyncSender<Request>,
    rx: Receiver<Response>,
}

impl Backend {
    const CHANNEL_BOUND: usize = 2048;

    fn spawn(ctx: Context) -> Self {
        let (req_tx, req_rx) = std::sync::mpsc::sync_channel(Self::CHANNEL_BOUND);
        let (res_tx, res_rx) = std::sync::mpsc::sync_channel(Self::CHANNEL_BOUND);
        crate::backend::spawn(ctx, res_tx, req_rx);
        Self { tx: req_tx, rx: res_rx }
    }
}

//
// =============================================================================
//

pub fn modal_error(dialog: &Modal, msg: &str) {
    error!("{msg}");
    dialog.dialog().with_title("Error").with_body(msg).with_icon(Icon::Error).open();
}

//
// =============================================================================
//

fn data_base_dir() -> Option<PathBuf> {
    let data_dir = std::env!("CARGO_PKG_NAME");
    let data_dir = dirs::data_dir().map(|p| p.join(data_dir))?;
    std::fs::create_dir_all(&data_dir).ok()?;
    Some(data_dir)
}

fn app_settings_file() -> Option<PathBuf> {
    data_base_dir().map(|p| p.join("settings.json"))
}

fn try_save_app_settings(settings: &GeneralSettings) {
    if let Some(data_dir) = app_settings_file() {
        if let Ok(app_settings) = serde_json::ser::to_string(settings) {
            std::fs::write(data_dir, app_settings).ok();
        }
    }
}

fn load_app_settings() -> Option<GeneralSettings> {
    let settings_file = app_settings_file()?;
    let settings: GeneralSettings =
        serde_json::de::from_reader(std::fs::File::open(settings_file).ok()?).ok()?;
    Some(settings)
}
