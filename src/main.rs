// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#![doc = include_str!("../README.md")]
// Hide console window on Windows when using release profile.
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

#[global_allocator]
static GLOBAL: mimalloc::MiMalloc = mimalloc::MiMalloc;

use std::sync::Arc;

use color_eyre::eyre::eyre;
use eframe::{egui::viewport::IconData, NativeOptions};
use tracing::error;
use tracing_subscriber::{fmt::format, EnvFilter};

mod app;
mod backend;
mod settings;

//
// =============================================================================
//

pub const MIN_WINDOW_WIDTH: f32 = 880.;
pub const MIN_WINDOW_HEIGHT: f32 = MIN_WINDOW_WIDTH / 16. * 9.;
const DEFAULT_WINDOW_WIDTH: f32 = 960.;
const DEFAULT_WINDOW_HEIGHT: f32 = DEFAULT_WINDOW_WIDTH * 0.618;

//
// =============================================================================
//

fn main() -> color_eyre::Result<()> {
    init_logging_and_eyre();

    let inner_size = [DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT];
    let min_inner_size = [MIN_WINDOW_WIDTH, MIN_WINDOW_HEIGHT];

    // Render icon.
    let mut font_db = resvg::usvg::fontdb::Database::new();
    font_db.load_system_fonts();
    font_db.load_system_fonts();
    let mut resvg_options = resvg::usvg::Options::default();
    *resvg_options.fontdb_mut() = font_db;
    let icon_svg =
        resvg::usvg::Tree::from_str(include_str!("../artwork/icon.svg"), &resvg_options)?;
    let icon_size = 256;
    let mut icon_pixmap =
        resvg::tiny_skia::Pixmap::new(icon_size, icon_size).expect("allocating icon pixmap failed");
    let transform = resvg::tiny_skia::Transform::from_scale(
        icon_size as f32 / icon_svg.size().width(),
        icon_size as f32 / icon_svg.size().height(),
    );
    resvg::render(&icon_svg, transform, &mut icon_pixmap.as_mut());
    let icon = Arc::new(IconData { rgba: icon_pixmap.take(), width: icon_size, height: icon_size });

    // Set window options.
    let mut native_options = NativeOptions::default();
    native_options.viewport = native_options
        .viewport
        .with_min_inner_size(min_inner_size)
        .with_inner_size(inner_size)
        .with_icon(icon);

    // Run app.
    if let Err(e) = eframe::run_native(
        std::env!("LONG_NAME"),
        native_options,
        Box::new(|cc| Box::new(app::App::new(cc))),
    ) {
        error!("{e}");
        Err(eyre!("{e}"))
    } else {
        Ok(())
    }
}

//
// =============================================================================
//

/// Initialize logging system and colorful error output.
///
/// # Panics
///
/// If this function is called more than once.
pub fn init_logging_and_eyre() {
    let env_filter = EnvFilter::try_from_default_env().unwrap_or_default();
    tracing_subscriber::fmt().event_format(format().compact()).with_env_filter(env_filter).init();
    color_eyre::install().expect("This function is only called once at the start of the program");
}
