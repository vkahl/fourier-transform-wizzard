// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use derive_more::Display;
use enum_iterator::Sequence;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GeneralSettings {
    pub dark_mode: DarkModeSettings,
    pub line_width: f32,
}

impl Default for GeneralSettings {
    fn default() -> Self {
        Self { dark_mode: DarkModeSettings::default(), line_width: 2. }
    }
}

#[derive(Debug, Clone, Copy, Default, Serialize, Deserialize, Sequence, Display)]
pub enum DarkModeSettings {
    #[default]
    System,
    On,
    Off,
}

impl DarkModeSettings {
    pub fn toggle(&mut self) {
        *self = match self {
            Self::System => Self::On,
            Self::On => Self::Off,
            Self::Off => Self::System,
        }
    }
}
