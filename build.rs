use std::path::PathBuf;

fn render_pngs(sizes: &[u32]) -> Vec<Vec<u8>> {
    println!("cargo:rerun-if-changed=artwork/icon.svg");
    println!("cargo:rerun-if-changed=artwork/about.svg");

    use resvg::usvg::Options;

    let icon_base_path =
        std::path::Path::new(&std::env::var("CARGO_MANIFEST_DIR").unwrap()).join("artwork");

    // Load and parse svg.
    let mut font_db = resvg::usvg::fontdb::Database::new();
    font_db.load_system_fonts();
    let mut resvg_options = Options::default();
    *resvg_options.fontdb_mut() = font_db;
    let icon_svg_string = std::fs::read_to_string(icon_base_path.join("icon.svg"))
        .expect("Reading icon svg file failed");
    let tree = resvg::usvg::Tree::from_str(&icon_svg_string, &resvg_options)
        .expect("Parsing icon svg failed");

    // Render pngs with different sizes.
    let mut pngs = vec![];
    for size in sizes.iter().copied() {
        let mut pixmap = resvg::tiny_skia::Pixmap::new(size, size)
            .expect("Initializing the pixmap for svg rendering");
        let transform = resvg::tiny_skia::Transform::from_scale(
            size as f32 / tree.size().width(),
            size as f32 / tree.size().height(),
        );
        resvg::render(&tree, transform, &mut pixmap.as_mut());
        let png_data = pixmap.encode_png().expect("Error encoding icon pixmap to png");
        pngs.push(png_data);
    }

    // Render and save about-page logo png.
    let about_svg_string = std::fs::read_to_string(icon_base_path.join("about.svg"))
        .expect("Reading about svg file failed");

    let tree = resvg::usvg::Tree::from_str(&about_svg_string, &resvg_options)
        .expect("Parsing icon svg failed");
    #[allow(clippy::cast_possible_truncation, clippy::cast_sign_loss)]
    let mut pixmap = resvg::tiny_skia::Pixmap::new(
        tree.size().width().ceil() as u32,
        tree.size().height().ceil() as u32,
    )
    .expect("Initializing the pixmap for svg rendering");
    resvg::render(&tree, resvg::tiny_skia::Transform::default(), &mut pixmap.as_mut());
    pixmap.save_png(gitignore_dir().join("about.png")).expect("Error encoding icon pixmap to png");

    pngs
}

fn write_dependencies_json() {
    println!("cargo:rerun-if-changed=Cargo.lock");
    println!("cargo:rerun-if-changed=Cargo.toml");
    println!("cargo:rerun-if-changed=gitignore/dependencies.json");

    use cargo_license::{get_dependencies_from_cargo_lock, GetDependenciesOpt};

    #[allow(clippy::default_trait_access)]
    let dependencies = get_dependencies_from_cargo_lock(
        Default::default(),
        GetDependenciesOpt {
            avoid_dev_deps: true,
            avoid_build_deps: true,
            direct_deps_only: true,
            root_only: false,
        },
    )
    .unwrap();

    let deps_json = serde_json::to_string_pretty(&dependencies).unwrap();

    let dependencies_file = gitignore_dir().join("dependencies.json");
    std::fs::write(dependencies_file, deps_json).unwrap();
}

/// Create a directory called "gitignore" (which is listed in the .gitignore
/// file) and return its path
fn gitignore_dir() -> PathBuf {
    let path =
        std::path::Path::new(&std::env::var("CARGO_MANIFEST_DIR").unwrap()).join("gitignore");
    std::fs::create_dir(&path).ok();
    path
}

fn main() {
    println!("cargo:rerun-if-changed=build.rs");

    write_dependencies_json();

    // Render pngs
    let sizes = [16, 32, 48, 64, 128, 256, 512];
    let pngs = render_pngs(&sizes);

    #[cfg(target_os = "windows")]
    main_windows(pngs);
    #[cfg(target_os = "linux")]
    main_linux(pngs, &sizes);
}

#[cfg(target_os = "windows")]
fn main_windows(pngs: Vec<Vec<u8>>) {
    // Assemble ico from pngs
    let mut icon_dir = ico::IconDir::new(ico::ResourceType::Icon);
    for png_data in pngs {
        let icon_image = ico::IconImage::read_png(png_data.as_slice())
            .expect("Error creating the icon image from png");
        icon_dir.add_entry(
            ico::IconDirEntry::encode(&icon_image)
                .expect("Error encoding icon image into icon dir entry"),
        );
    }

    // Save the icon
    let icon_out_dir = std::path::Path::new(&std::env::var("OUT_DIR").unwrap()).join("icon");
    let icon_ico_path = icon_out_dir.join("icon.ico");
    // Try creating the output dir if it doesn't exist
    std::fs::create_dir(icon_out_dir).ok();
    let icon_ico_file = std::fs::File::create(&icon_ico_path).expect("Error creating ico file");
    icon_dir.write(icon_ico_file).expect("Error writing icon data into ico file");

    // Embed the icon in the executable
    let mut res = winresource::WindowsResource::new();

    res.set_language(0x0407); // German
    res.set("ProductName", std::env!("DISPLAY_NAME"));
    res.set("FileDescription", std::env!("CARGO_PKG_DESCRIPTION"));
    res.set("LegalCopyright", "Released under MPL-2.0");
    res.set("CompanyName", "Valentin Kahl");

    res.set_icon(&icon_ico_path.to_string_lossy());
    res.compile().expect("Error compiling the resource file");
}

#[cfg(target_os = "linux")]
fn main_linux(pngs: Vec<Vec<u8>>, sizes: &[u32]) {
    // Save pngs
    let png_out_dir = std::path::Path::new(&std::env::var("OUT_DIR").unwrap()).join("icon");

    if let Err(e) = std::fs::create_dir(&png_out_dir) {
        // Only panic if a different error then `AlreadyExists` occurs,
        // otherwise ignore the error.
        #[allow(clippy::manual_assert)]
        if e.kind() != std::io::ErrorKind::AlreadyExists {
            panic!("Error creating png directory: {e:?}");
        }
    }

    // Save png files for cargo bundle
    for (size, png_data) in sizes.iter().zip(pngs) {
        let file_name = format!("icon_{size}.png");
        let png_path = png_out_dir.join(&file_name);
        std::fs::write(png_path, png_data).unwrap();
    }
}
