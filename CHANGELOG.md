# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.4] - 2024-10-22

### Added

- Display the index of the current component.
- UI-Zoom functionality (via "View"-menu or hotkeys).

### Changed

- Some performance optimizations.
- Changed "Edit" menu caption to "View".

## [0.1.3] - 2023-11-08

### Changed

- Updated wavers dependency, after a bug fix ([github
  issue](https://github.com/jmg049/wavers/issues/9)).

### Added

- Allow opening wav-files directly with FTW, passing the path as first
  commandline argument.

### Fixed

- Correct error message if the provided file is too long.

## [0.1.2] - 2023-11-06

### Added

- A hyperlink to the code repository in the about window.

### Changed

- At the last, highest frequency node, hide the property display and the current
  frequency node graph (instead of showing `inf` as frequency), since there is
  no further frequency being added.
- The layout of the property display so it takes up less vertical space
  (frequency, magnitude, phase).
- Reduced default plot line width to 2 pixels.
- Reduced max. allowed audio file length to 65536 samples.

### Fixed

- Added a workaround to a bug in the wavers library. Before this fix, some wav
  files weren't read in entirely ([github
  issue](https://github.com/jmg049/wavers/issues/9)).

## [0.1.1] - 2023-11-04

### Added

- A slider in the edit menu for changing the plot line width.
- A navigation help window accessible via the help menu.

### Changed

- Allow signals that are up to 2 seconds long.
- Display a spinner while calculating the next step, if it takes too long
  (instead of no visual feedback at all)
- Even more fun logo (added drop shadow, changed color scheme to slightly
  desaturated comic book style colors).
- Plot line colors for overall more consistent color scheme.

### Fixed

- The frequency calculation for signals with an uneven amount of frames.
- A bug where the displayed magnitude was doubled for the highest frequency node
  of signals with an even amount of frames.
- Allow displaying the last node (finished "reconstruction"), just displaying
  `inf` as next frequency value.
